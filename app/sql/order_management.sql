-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.17 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para order_management
CREATE DATABASE IF NOT EXISTS `order_management` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `order_management`;


-- Volcando estructura para tabla order_management.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(100) DEFAULT NULL,
  `customer_ref` varchar(100) DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ref` (`ref`),
  KEY `customer_ref` (`customer_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla order_management.orders: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `ref`, `customer_ref`, `total_price`, `created_at`, `updated_at`) VALUES
	(1, 'rwerwefsdf', 'customer_1', 100, '2017-11-29 15:47:39', '2017-11-29 15:47:40'),
	(2, 'dfsdfsdfsd', 'cusomer_2', 200, '2017-11-29 15:57:07', '2017-11-29 15:57:09');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Volcando estructura para tabla order_management.order_items
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_price` double NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla order_management.order_items: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` (`id`, `unit_price`, `product_id`, `order_id`, `quantity`, `created_at`, `updated_at`) VALUES
	(1, 10, 1, 1, 5, '2017-11-29 15:59:53', '2017-11-29 15:59:54'),
	(2, 7, 2, 1, 8, '2017-11-29 16:00:20', '2017-11-29 16:00:21'),
	(3, 5, 2, 2, 4, '2017-11-29 16:01:42', '2017-11-29 16:01:44');
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;


-- Volcando estructura para tabla order_management.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ref` (`ref`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla order_management.products: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `ref`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'fdfsdfewrr234', 'Lapicero', '2017-11-28 22:58:49', '2017-11-28 22:58:51'),
	(2, 'dsdfsdfsdfsdfsd', 'Borrador', '2017-11-29 14:45:55', '2017-11-29 14:45:57'),
	(3, 'tsdfsdfghrtret', 'Test2', '2017-11-29 19:22:40', '2017-11-29 19:22:40'),
	(4, 'ASSSSSDDDDDDD', 'TEST3', '2017-11-29 19:24:33', '2017-11-29 19:24:33');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
