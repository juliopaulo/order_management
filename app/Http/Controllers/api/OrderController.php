<?php

namespace App\Http\Controllers\Api;

use Order;
use Validator;
use Invitation;
use Input;

class OrderController extends ApiController
{

    public function __construct()
    {

    }

    /**
     * **GET /api/1.0/order**
     * 
     * Retorna los orders
     * 
     * @return \App\Models\Order
     */
    public function getIndex()
    {
        return Order::get();
    }

    /**
     * **GET /api/1.0/order/{orderId}**
     *
     * Retorna los detalles del order
     *
     * @return \App\Models\Order
     */
    public function getView($orderId)
    {
        return $this->findModel('Order', $orderId);
    }


}