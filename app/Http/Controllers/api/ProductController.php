<?php

namespace App\Http\Controllers\Api;

use Product;
use Validator;
use Invitation;
use Input;

class ProductController extends ApiController
{

    public function __construct()
    {

    }

    /**
     * **GET /api/1.0/product**
     * 
     * Retorna los productos
     * 
     * @return \App\Models\Product
     */
    public function getIndex()
    {
        return Product::get();
    }

    /**
     * **GET /api/1.0/product/{productId}**
     *
     * Retorna los detalles del producto
     *
     * @return \App\Models\Product
     */
    public function getView($productId)
    {
        return $this->findModel('Product', $productId);
    }

    /**
     * **POST /post/1.0/product**
     *
     * - `name (required|max:255)`
     * - `ref (required|max:255)`
     *
     * @return \App\Models\Product
     */
    public function postIndex()
    {
        $validator = Validator::make($this->inputAll(), [
            'name'     => 'required|max:255',
            'ref'     => 'required|max:255'
        ]);

        if ($validator->fails()) return $this->errors($validator->errors()->getMessages());

        $product = Product::create([
            'name'     => Input::get('name'),
            'ref'    => Input::get('ref')
        ]);

        return $product;
    }



}