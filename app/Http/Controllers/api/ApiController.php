<?php

namespace App\Http\Controllers\Api;

use User;
use Input;
use Config;
use Response;
use Authorizer;
use App\Http\Controllers\Controller;
use LucaDegasperi\OAuth2Server\Exceptions\NoActiveAccessTokenException;

class ApiController extends Controller
{
    public function errors($errors = array())
    {
        return Response::json(['errors' => $errors], 400);
    }

    /**
     * Busca un modelo a partir de su nombre e id. Valida además que exista ese $id y que, en caso de tener
     * un usuario como propietario, revisar que correspondan.
     *
     * @param $name
     * @param $id
     * @return \App\Models\Model
     */
    protected function findModel($name, $id)
    {
        $model = $name::find($id);
        $attr  = strtolower($name);

        if (empty($model))
        {
            return $this->errors(["{$attr}_id" => trans('validation.exists', ['attribute' => "{$attr} id"])]);
        }

    /**    if (isset($model->user_id) and $model->user_id != $this->getUserId())
        {
            return $this->errors(["{$attr}_id" => trans('validation.exists', ['attribute' => "{$attr} id"])]);
        }
**/
        return $model;
    }

    protected function inputAll(array $extra = [])
    {
        return array_merge(Input::all(), ['user_id' => ''], $extra);
    }


    protected function getUser()
    {
        return User::find($this->getUserId());
    }

/**    protected function getUserId()
    {
        try
        {
            return Authorizer::getResourceOwnerId();
        }
        catch (NoActiveAccessTokenException $e)
        {
            return null;
        }
    }
**/

}