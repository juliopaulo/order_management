<?php

namespace App\Http\Controllers\Api;

use OrderItem;
use Validator;
use Invitation;
use Input;

class OrderItemController extends ApiController
{

    public function __construct()
    {

    }

    /**
     * **GET /api/1.0/orderitem/{orderId}**
     *
     * Retorna los detalles del orderItem
     *
     * @return \App\Models\OrderItem
     */
    public function getView($orderId)
    {
        return OrderItem::where('order_id', $orderId)->get();
    }


}