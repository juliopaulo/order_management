<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'api'], function ()
{
    // Products routes
	Route::get('product/{productId}',    'Api\ProductController@getView');
    Route::controller('product', Api\ProductController::class);

    Route::get('order/{orderId}',    'Api\OrderController@getView');
    Route::controller('order', Api\OrderController::class);

    Route::get('orderitem/{orderId}',    'Api\OrderItemController@getView');


});
