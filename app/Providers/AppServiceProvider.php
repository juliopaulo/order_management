<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('user',         \App\Models\User::class);
        $this->app->bind('order',         \App\Models\Order::class);
        $this->app->bind('orderItem',         \App\Models\OrderItem::class);
        $this->app->bind('product',         \App\Models\Product::class);

    }
}
