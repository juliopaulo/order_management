<?php

/*
|--------------------------------------------------------------------------
| Extra Content
|--------------------------------------------------------------------------
*/

if ( ! function_exists('app_url'))
{
    function app_url($path = '/', $params = array())
    {
        return url($path, $params, starts_with(Config::get('app.url'), 'https'));
    }
}

if ( ! function_exists('config'))
{
    function config($key, $default = null)
    {
        return Config::get($key, $default);
    }
}

if ( ! function_exists('get_domain'))
{
    function get_domain($url)
    {
        return preg_replace('/https?:\/\//', '', $url);
    }
}

if ( ! function_exists('date_string'))
{
    function date_string($date, $timestamp = false, $format = 'd/m/Y H:i')
    {
        $carbon = $timestamp ? \Carbon\Carbon::createFromTimestamp($date) : \Carbon\Carbon::parse($date);

        return $carbon->format($format);
    }
}

if ( ! function_exists('time_string'))
{
    function time_string($date, $timestamp = false)
    {
        return date_string($date, $timestamp, 'H:i');
    }
}

if ( ! function_exists('currency_string'))
{
    function currency_string($amount, $currency = 'EUR')
    {
        return str_replace(',00', '', number_format($amount, 2, ',', '.') . '€');
    }
}

if ( ! function_exists('strip_spaces'))
{
    function strip_spaces($string)
    {
        return preg_replace('/\s+/', '', $string);
    }
}

if ( ! function_exists('generate_sms_code'))
{
    function generate_sms_code()
    {
        return mt_rand(100000, 999999);
    }
}

if ( ! function_exists('notify'))
{
    function notify($key, $attributes = [])
    {
        return trans("notification.{$key}", $attributes);
    }
}