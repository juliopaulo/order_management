<?php

namespace App\Models;

class Product extends Model
{
    //
    protected $table = 'products';

      protected $fillable = [
        'ref', 'name'
    ];

    protected $hidden = [
         'created_at', 'updated_at'
    ];


}
