<?php

namespace App\Models;

class Order extends Model
{
    //
    protected $table = 'orders';

      protected $fillable = [
        'customer_ref', 'total_price'
    ];

    protected $hidden = [
         'created_at', 'updated_at', 'order_item'
    ];


    public function orderItem()
    {
        return $this->hasOne('App\Models\OrderItem', 'order_id')->latest();
    }


}
