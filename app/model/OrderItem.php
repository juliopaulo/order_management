<?php

namespace App\Models;

class OrderItem extends Model
{
    //
    protected $table = 'order_items';

      protected $fillable = [
        'unit_price', 'quantity'
    ];

    protected $hidden = [
         'order_id', 'product_id' ,'created_at', 'updated_at'
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }


}
