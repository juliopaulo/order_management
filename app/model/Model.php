<?php

namespace App\Models;

use Eloquent;
use Validator;
use Ferranfg\Laravextra\Models\BaseModel;

class Model extends Eloquent
{
    use BaseModel;    

    protected $errors;

    public function uploadImage($upload, $type, $unique = true)
    {
        $image = $unique && $this->image ? $this->image : new Image;

        if ($upload)
        {
            $content = (new Upload)->make($upload);

            $image->imageable_type = $type;
            $image->imageable_id   = $this->id;
            $image->content        = (string) $content->encode('jpg', 90);

            $image->save();

            $this->setRelation('image', $image);
        }

        return $image;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function validator($callback)
    {
        $validator = Validator::make($this->getAttributes(), []);

        $validator->after(function($validator) use ($callback)
        {
            $callback($validator);
        });

        $this->errors = $validator->errors();

        return $validator->fails();
    }

}